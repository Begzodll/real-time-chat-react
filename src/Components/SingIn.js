import firebase from "firebase";
import React from "react";

function SignIn() {
    const auth = firebase.auth();

    const signInWithGoogle = () => {
        const provider = new firebase.auth.GoogleAuthProvider();
        auth.signInWithPopup(provider);
    }

    return (
        <div className={'singIn'}>
            <button className="sign-in" onClick={signInWithGoogle}>Sign in with Google</button>
            <div className={'rules'}>Do not violate the community guidelines or you will be banned for life!</div>
        </div>
    )

}
export default SignIn