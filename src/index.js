import * as serviceWorker from './serviceWorker';
import React from 'react';

import ReactDOM from 'react-dom';

import App from './App';

import './firebase';
import './assets/index.css'



ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
