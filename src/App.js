import React from 'react';
import SingOut from './Components/SingOut';
import SignIn from "./Components/SingIn";
import ChatRoom from "./Components/chatRoom";

import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/analytics';

import {useAuthState} from 'react-firebase-hooks/auth';
import './assets/App.css';


function App() {

    const auth = firebase.auth();
    const [user] = useAuthState(auth);

    return (
        <div className={'App'}>
            <header>
                <h1 className={'hidden'}>International Chat</h1>
                <SingOut/>
            </header>

            <section>
                {user ? <ChatRoom/> : <SignIn/>}
            </section>

        </div>
    );
}

export default App;
