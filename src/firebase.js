import firebase from "firebase";

firebase.initializeApp({
    apiKey: process.env.REACT_APP_API_KEY_REACT_CHAT ,
    authDomain:process.env.REACT_APP_AUTH_DOMAIN_REACT_CHAT ,
    projectId: process.env.REACT_APP_PROJECT_ID_REACT_CHAT,
    storageBucket: process.env.REACT_APP_STORAGE_BECKET_REACT_CHAT,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID_USER_ID ,
    appId: process.env.REACT_APP_APP_ID_PROJECT_ID ,
    measurementId: process.env.REACT_APP_MEASUREMENT_ID_PROJECT
})